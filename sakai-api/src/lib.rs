use reqwest::{Client, Method};

use std::collections::HashMap;

#[macro_use]
extern crate serde;

pub struct SakaiApi {
    cookie: String,
    sakai_baseurl: String,
    client: Client,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Membership {
    id: String,
    member_role: String,
    location_reference: String,
}

impl Membership {
    pub fn id(&self) -> &str {
        &self.id
    }
    pub fn location_reference(&self) -> &str {
        &self.location_reference
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Memberships {
    pub membership_collection: Vec<Membership>,
}

impl Memberships {
    pub fn items(&self) -> &[Membership] {
        &self.membership_collection
    }
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Attachment {
    r#ref: String,
    size: u32,
    r#type: String,
}

impl Attachment {
    pub fn mime_type(&self) -> &str {
        &self.r#type
    }

    pub fn size(&self) -> u32 {
        self.size
    }

    pub fn name(&self) -> &str {
        &self.r#ref
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, Copy, PartialEq, PartialOrd)]
#[serde(rename_all = "camelCase")]
pub struct Time {
    epoch_second: u32,
    nano: u32,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "UPPERCASE")]
pub enum AssignmentStatus {
    Open,
    Closed,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Assignment {
    access: String,
    attachments: Vec<Attachment>,
    author: String,
    open_time: Time,
    close_time: Time,
    id: String,
    instructions: Option<String>,
    max_grade_point: Option<String>,
    status: AssignmentStatus,
    title: String,
    allow_resubmission: bool,
    anonymous_grading: bool,
    draft: bool,
    entity_id: String,
    #[serde(rename = "entityURL")]
    entity_url: String,
}

impl Assignment {
    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn instructions(&self) -> Option<&str> {
        self.instructions.as_ref().map(|s| s.as_str())
    }

    pub fn author(&self) -> &str {
        &self.author
    }

    pub fn open_time(&self) -> Time {
        self.open_time
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Assignments {
    assignment_collection: Vec<Assignment>,
}

impl Assignments {
    pub fn items(&self) -> &[Assignment] {
        &self.assignment_collection
    }
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Announcement {
    announcement_id: String,
    attachments: Vec<Attachment>,
    title: String,
    body: String,
    entity_reference: String,
    created_on: i64,
}

impl Announcement {
    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn body(&self) -> &str {
        &self.body
    }

    pub fn attachments(&self) -> &[Attachment] {
        &self.attachments
    }
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Announcements {
    announcement_collection: Vec<Announcement>,
}

impl Announcements {
    pub fn items(&self) -> &[Announcement] {
        &self.announcement_collection
    }
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct User {
    created_date: u64,
    display_id: String,
    display_name: String,
    eid: String,
    email: String,
    id: String,
    props: HashMap<String, String>,
}

impl User {
    pub fn display_id(&self) -> &str {
        &self.display_id
    }

    pub fn display_name(&self) -> &str {
        &self.display_name
    }

    pub fn email(&self) -> &str {
        &self.email
    }

    pub fn user_name(&self) -> &str {
        &self.eid
    }

    pub fn id(&self) -> &str {
        &self.id
    }

    pub fn properties(&self) -> &HashMap<String, String> {
        &self.props
    }
}

impl SakaiApi {
    pub fn new(sakai_baseurl: String, cookie: String) -> SakaiApi {
        let client = Client::new();

        SakaiApi {
            sakai_baseurl,
            client,
            cookie,
        }
    }

    pub fn new_with_client(client: Client, sakai_baseurl: String, cookie: String) -> SakaiApi {
        SakaiApi {
            sakai_baseurl,
            client,
            cookie,
        }
    }

    fn prepare_request(&self, path: &str) -> reqwest::RequestBuilder {
        let url = format!("{}/{}", self.sakai_baseurl, path);
        self.client
            .request(Method::GET, url)
            .header("Cookie", &self.cookie)
    }

    fn prepare_post_request(&self, path: &str) -> reqwest::RequestBuilder {
        let url = format!("{}/{}", self.sakai_baseurl, path);
        self.client
            .request(Method::POST, url)
            .header("Cookie", &self.cookie)
    }

    pub async fn memberships(&self) -> reqwest::Result<Memberships> {
        self.client
            .execute(
                self.prepare_request("direct/membership/fastroles.json")
                    .build()?,
            )
            .await?
            .json::<Memberships>()
            .await
    }

    pub async fn assignment(&self, id: u32) -> reqwest::Result<Assignment> {
        self.client
            .execute(
                self.prepare_request(&format!("direct/assignment/item/{}.json", id))
                    .build()?,
            )
            .await?
            .json::<Assignment>()
            .await
    }

    pub async fn assignments(
        &self,
        memership_location_reference: &str,
    ) -> reqwest::Result<Assignments> {
        self.client
            .execute(
                self.prepare_request(&format!(
                    "direct/assignment/{}.json",
                    memership_location_reference
                ))
                .build()?,
            )
            .await?
            .json::<Assignments>()
            .await
    }

    pub async fn my_assignments(&self) -> reqwest::Result<Assignments> {
        self.client
            .execute(self.prepare_request("direct/assignment/my.json").build()?)
            .await?
            .json::<Assignments>()
            .await
    }

    pub async fn announcements(
        &self,
        membership_location_reference: &str,
    ) -> reqwest::Result<Announcements> {
        self.client
            .execute(
                self.prepare_request(&format!(
                    "direct/announcement/{}.json",
                    membership_location_reference,
                ))
                .build()?,
            )
            .await?
            .json::<Announcements>()
            .await
    }

    pub async fn announcement(
        &self,
        membership_id: &str,
        msg_id: &str,
    ) -> reqwest::Result<Announcement> {
        self.client
            .execute(
                self.prepare_request(&format!(
                    "direct/announcement/message/{}/{}",
                    membership_id, msg_id
                ))
                .build()?,
            )
            .await?
            .json::<Announcement>()
            .await
    }

    pub async fn join_site(&self, site_id: &str) -> reqwest::Result<()> {
        self.client
            .execute(
                self.prepare_post_request(&format!("direct/membership/join/site/{}", site_id))
                    .build()?,
            )
            .await?
            .json::<()>()
            .await
    }

    pub async fn current_user(&self) -> reqwest::Result<User> {
        self.client
            .execute(self.prepare_request("direct/user/current.json").build()?)
            .await?
            .json::<User>()
            .await
    }

    pub async fn grades(&self, course_id: &str, gradable_id: &str) -> reqwest::Result<()> {
        self.client
            .execute(
                self.prepare_request("direct/assignment/grades")
                    .query(&[("courseId", course_id), ("gradableId", gradable_id)])
                    .build()?,
            )
            .await?
            .json::<()>()
            .await
    }

    pub async fn set_grade(
        &self,
        course_id: &str,
        gradable_id: &str,
        grade: &str,
        student_id: &str,
        submission_id: &str,
    ) -> reqwest::Result<()> {
        self.client
            .execute(
                self.prepare_post_request("direct/assignment/setGrade")
                    .query(&[
                        ("courseId", course_id),
                        ("gradableId", gradable_id),
                        ("grade", grade),
                        ("studentId", student_id),
                        ("submissionId", submission_id),
                    ])
                    .build()?,
            )
            .await?
            .json::<()>()
            .await
    }
}

#[cfg(test)]
mod integration_tests {
    use crate::*;
    use std::env::var;

    fn get_test_api() -> SakaiApi {
        SakaiApi::new(
            "https://mycampus.imp.fu-berlin.de/".to_string(),
            var("SAKAI_SESSION_COOKIE").expect("SAKAI_SESSION_COOKIE not set in the environment"),
        )
    }
    #[tokio::test]
    async fn get_memberships() {
        let api = get_test_api();
        let memberships = api.memberships();
        println!("{:#?}", memberships.await.unwrap());
    }

    #[tokio::test]
    async fn get_assignments() {
        let api = get_test_api();
        let memberships = api.memberships().await.unwrap();
        let assignments = api
            .assignments(memberships.items().first().unwrap().location_reference())
            .await
            .unwrap();
        println!("{:#?}", assignments);
    }

    #[tokio::test]
    async fn get_my_assignments() {
        let api = get_test_api();
        let assignments = api.my_assignments().await.unwrap();
        println!("{:#?}", assignments);
    }

    #[tokio::test]
    async fn get_announcements() {
        let api = get_test_api();
        let memberships = api.memberships().await.unwrap();
        let announcements = api
            .announcements(memberships.items().first().unwrap().id())
            .await
            .unwrap();
        println!("{:#?}", announcements);
    }
}
