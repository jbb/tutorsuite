use rocket::http::{Cookie, CookieJar};
use rocket::{get, post, launch, response::Redirect, routes, uri, State, form::{FromForm, Form}};
use rocket_dyn_templates::Template;

use serde::Serialize;

use std::collections::HashMap;

use sakai_api::SakaiApi;

use reqwest::Client;

const SAKAI_BASEURL: &str = "https://mycampus.imp.fu-berlin.de/";
const SESSION_COOKIE: &str = "SakaiSession";

#[get("/logout")]
fn logout(cookies: &CookieJar<'_>) -> Redirect {
    cookies.remove(Cookie::named(SESSION_COOKIE));
    Redirect::to(uri!(home))
}

#[get("/login")]
fn login() -> Template {
    let context = HashMap::<String, String>::new();
    Template::render("login", &context)
}

fn get_sakai_session(cookies: &CookieJar<'_>, client: &State<Client>) -> Option<SakaiApi> {
    Some(SakaiApi::new_with_client(
        client.inner().clone(),
        SAKAI_BASEURL.to_string(),
        cookies.get(SESSION_COOKIE)?.value().to_string(),
    ))
}

#[derive(Serialize)]
#[serde(tag = "type")]
enum UserVisibleError {
    NotLoggedIn,
    FetchFailed,
}

#[get("/")]
async fn home(cookies: &CookieJar<'_>, client: &State<Client>) -> Template {
    #[derive(Serialize, Default)]
    struct Context {
        user_display_name: String,
        user_email: String,
        error: Option<UserVisibleError>,
        properties: HashMap<String, String>,
    }
    let mut context = Context::default();

    match get_sakai_session(cookies, client) {
        Some(sakai) => match sakai.current_user().await {
            Ok(user) => {
                context.user_display_name = user.display_name().to_string();
                context.user_email = user.email().to_string();
                context.properties = user.properties().clone();
            }
            Err(_e) => {
                context.error = Some(UserVisibleError::FetchFailed);
            }
        },
        None => {
            context.error = Some(UserVisibleError::NotLoggedIn);
        }
    }
    Template::render("home", &context)
}

#[get("/assignments")]
async fn assignments(cookies: &CookieJar<'_>, client: &State<Client>) -> Template {
    #[derive(Serialize)]
    struct Context {
        assignments: Vec<sakai_api::Assignment>,
    }
    let sakai = get_sakai_session(cookies, client).unwrap();

    let mut context = Context {
        assignments: sakai.my_assignments().await.unwrap().items().to_vec(),
    };

    context
        .assignments
        .sort_by(|a, b| a.open_time().partial_cmp(&b.open_time()).unwrap().reverse());

    Template::render("assignments", &context)
}

#[derive(FromForm)]
struct LoginForm {
    session: String
}

#[post("/login", data = "<form>")]
fn do_login(form: Form<LoginForm>, cookies: &CookieJar<'_>) -> Redirect {
    cookies.add(Cookie::new(SESSION_COOKIE, form.session.clone()));
    Redirect::to(uri!(home))
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(Template::fairing())
        .manage(Client::new())
        .mount("/", routes![login, do_login, home, logout, assignments])
}
