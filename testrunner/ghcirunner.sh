#!/usr/bin/env bash

set -e
set -u

source_file="$1"
function_name="$2"
return_type="$3"
function_arguments="${@:4}"

cat << EOF | ghci
:load ${source_file}
let ret :: ${return_type}; ret = ${function_name} ${function_arguments}
ret
EOF
