import Data.Char (toUpper)

{- 1. Aufgabe
a) Die Funktionsdefinition ist inkorrekt, da sie bei negativen eingaben falsche Ergebnisse liefert.
b)
-}
ungerade n = rem (abs n) 2 == 1

{- 2. Aufgabe -}
type Center  = (Double, Double)         -- Mittelpunkt eines Kreises
type Radius  = Double
type Circle  = (Center, Radius)         -- Mittelpunkt und Radius eines Kreises

area :: Circle -> Double
area (center, radius) = pi * (radius ** 2)

perimeter :: Circle -> Double
perimeter (center, radius) = 2 * pi * radius

equal :: Circle -> Circle -> Bool               -- Überprüft, ob die Kreise identisch sind (Position + Große)
equal ((x1, y1), r1) ((x2, y2), r2) = x1 == x2 && y1 == y2 && r1 == r2

intersect :: Circle -> Circle -> Bool   -- Testet, ob die Kreise sich überschneiden
intersect ((x1, y1), r1) ((x2, y2), r2) = distance < (r1 + r2)
    where
        distance = sqrt (((x1 - x2) ** 2) + ((y1 - y2) ** 2))

-- Koordinatenursprung oben links
contain :: Circle -> Circle -> Bool     -- Testet, ob der erste Kreis den zweiten Kreis enthält
contain ((x1, y1), r1) ((x2, y2), r2) = lx2 >= lx1 && rx2 <= rx1 && oy2 >= oy1 && uy2 <= uy1
    where
        lx1 = x1 - r1
        rx1 = x1 + r1
        oy1 = y1 - r1
        uy1 = y1 + r1
        lx2 = x2 - r2
        rx2 = x2 + r2
        oy2 = y2 - r2
        uy2 = y2 + r2

{- Aufgabe 3 -}

{-- Funktionale Programmierung, U1, 2020/2021 Author: M. Esponda --}
paintChars f size = putStrLn (genChars f size)

genChars :: ((Int, Int, Int) -> Char) -> Int -> [Char]
genChars f size = paint size (map f [(x,y,size) | y <- [1..size], x <- [1..size]])
    where
        paint 0  []     = []
        paint 0 (c:cs)  = '\n' : (paint size (c:cs))
        paint n (c:cs)  = c: (paint (n-1) cs)

{-- Funktionsbeispiele für die 3.Aufgabe des 1.Übungsblattes   --}
diag (x,y,size) = if x==y then 'O' else '.'

quad (x,y,size) = if (x>s && x<3*s && y>s && y<3*s) then ' ' else '+'
    where
        s = div size 4

gitter (x,y,size) = if k || p  then ' ' else '0'
    where
        k = mod x space == 0
        p = mod y space == 0
        space = div size 4

rectangles :: (Int, Int, Int) -> Char
rectangles (x, y, size) = if richtigeHoehe||richtigeBreite then '*' else ' '
    where
        richtigeHoehe = (rem (ceiling ((fromIntegral y)/achtel)) 2) == 1
        achtel = fromIntegral size/8
        richtigeBreite = x<(div size 4) || x>(3*(div size 4))

diags :: (Int, Int, Int) -> Char
diags (x, y, _) = do
    if (x `mod` 10 == y `mod` 10)
       then ' '
       else '0'

diamon :: (Int, Int, Int) -> Char
diamon (x, y, size) = do
    if (if(x<center)
       then if (y<center)
            then obenLinks
            else untenLinks
            else if (y<center)
                then obenRechts
                else untenRechts
       )
        then ' '
        else '0'
    where
        center = div size 2
        obenLinks = (center-x)>=y
        obenRechts = (x-center)>=(y)
        untenLinks = x<=(y-center)
        untenRechts = (center-(x-center))<=(y-center)

distance :: (Int, Int) -> (Int, Int) -> Float
distance (x1, y1) (x2, y2) = do
    let xd = x1 - x2
    let yd = y1 - y2
    sqrt ((fromIntegral xd ** 2) + (fromIntegral yd ** 2))

circle :: (Int, Int, Int) -> Char
circle (x, y, size) = do
    if dis < innerRadius
        then ' '
        else if dis < outerRadius
            then '.'
            else '*'
    where
        middle = (size `div` 2, size `div` 2)
        dis = distance middle (x, y)
        innerRadius = fromIntegral size * 0.3
        outerRadius = fromIntegral size * 0.48

flag::(Int, Int, Int) -> Char
flag (x, y, size) = if mitte then ' ' else (if (y < center) then (if (x < center) then obenLinks else obenRechts) else (unten))
    where
        zwanzigstel = fromIntegral size / 20
        center = div size 2
        obenLinks = if ((rem (ceiling ((fromIntegral x) / zwanzigstel)) 2) == 1) then '|' else ' '
        obenRechts = if ((rem (ceiling ((fromIntegral y) / zwanzigstel)) 2) == 1) then '*' else ' '
        unten = if ((rem (ceiling ((fromIntegral x) / zwanzigstel)) 2) == 1) then '+' else ' '
        mitte = (x > (div size 4)) && (x < (3 * (div size 4))) && (y > (div size 4)) && (y < (3 * (div size 4)))
        zehntel = div size 10

{- Aufgabe 4 -}

digit2GermanWord :: Int -> String
digit2GermanWord 0 = "null"
digit2GermanWord 1 = "eins"
digit2GermanWord 2 = "zwei"
digit2GermanWord 3 = "drei"
digit2GermanWord 4 = "vier"
digit2GermanWord 5 = "fünf"
digit2GermanWord 6 = "sechs"
digit2GermanWord 7 = "sieben"
digit2GermanWord 8 = "acht"
digit2GermanWord 9 = "neun"
digit2GermanWord 10 = "zehn"
digit2GermanWord 20 = "zwanzig"
digit2GermanWord 30 = "dreißig"
digit2GermanWord 40 = "vierzig"
digit2GermanWord 50 = "fünfzig"
digit2GermanWord 60 = "sechzig"
digit2GermanWord 70 = "siebzig"
digit2GermanWord 80 = "achzig"
digit2GermanWord 90 = "neunzig"

capitalize :: String -> String
capitalize (x:xs) = [toUpper x] ++ xs

num2GermanWords :: Int -> String
num2GermanWords 11 = "Elf"
num2GermanWords 12 = "Zwölf"
num2GermanWords num = vorzeichen ++ capitalize digits
    where
        vorzeichen = if num < 0 then "minus " else ""
        absnum = abs num
        firstDigit = absnum `rem` 10
        secondDigit = (absnum `div` 10) * 10
        digits = if absnum < 10
                    then digit2GermanWord absnum
                    else if firstDigit == 0
                        then digit2GermanWord secondDigit
                        else if firstDigit == 1
                            then "Einund" ++ digit2GermanWord secondDigit
                            else if absnum < 20
                                then digit2GermanWord firstDigit ++ digit2GermanWord secondDigit
                                else digit2GermanWord firstDigit ++ "und" ++ digit2GermanWord secondDigit
