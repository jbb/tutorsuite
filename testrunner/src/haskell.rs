use std::process::Command;

use crate::{ArgumentValue, TestRunner};

pub struct HaskellTestRunner;
impl TestRunner for HaskellTestRunner {
    fn run(
        &self,
        script: &str,
        function_name: &str,
        return_type: &str,
        function_args: &str,
    ) -> (String, String) {
        let output = Command::new("./ghcirunner.sh")
            .arg(script)
            .arg(function_name)
            .arg(return_type)
            .arg(function_args)
            .output()
            .unwrap();

        let stderr = String::from_utf8(output.stderr).unwrap();
        let stdout = String::from_utf8(output.stdout).unwrap();
        (
            stdout.replace(script, "<SOURCE FILE>"),
            stderr.replace(script, "<SOURCE FILE>"),
        )
    }

    fn serialize_arguments<T: ArgumentValue>(&self, args: &[(T, String)]) -> String {
        let mut serialized_args = Vec::<String>::new();

        for (value, data_type) in args {
            serialized_args.push(format!("({} :: {})", value, data_type))
        }

        serialized_args.join(" ")
    }
}
