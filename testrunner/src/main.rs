use clap::builder::{Arg, Command};
use core::fmt::{Debug, Display};
use rand::{thread_rng, Rng};
use std::fs;
use std::io::Write;
use std::rc::Rc;

mod haskell;
mod playbook;
use haskell::*;
use playbook::*;

const NUM_TESTS_PER_TASK: u16 = 10;

pub trait ArgumentValue: Debug + Display {}
impl<T> ArgumentValue for T where T: Debug + Display {}

pub trait TestRunner {
    fn run(
        &self,
        script: &str,
        function_name: &str,
        return_type: &str,
        function_args: &str,
    ) -> (String, String);
    fn serialize_arguments<T: ArgumentValue>(&self, args: &[(T, String)]) -> String;
}

fn main() {
    let args = Command::new("testrunner")
        .author("Jonah Brüchert <jbb@kaidan.im>")
        .version("0.1")
        .about("Runs automated tests on task submissions")
        .arg(Arg::new("task_spec").required(true))
        .arg(Arg::new("submission").required(true))
        .get_matches();

    let task_spec_path = args.get_one::<String>("task_spec").unwrap();
    let spec =
        serde_yaml::from_reader::<fs::File, Assignment>(fs::File::open(task_spec_path).unwrap())
            .unwrap();

    let submission_path = args.get_one::<String>("submission").unwrap();

    let runner = HaskellTestRunner;

    for task in spec.tasks {
        match task {
            Task::Function {
                name,
                arguments,
                output,
                points,
            } => {
                // Collect all possible arguments
                let mut argument_matrix: Vec<Vec<(Rc<dyn ArgumentValue>, String)>> = vec![];
                for i in 0..arguments.len() {
                    argument_matrix.push(Vec::new());
                    match &arguments[i].values {
                        ArgumentValues::Range { from, to } => {
                            for v in *from..*to {
                                argument_matrix[i]
                                    .push((Rc::from(v), arguments[i].data_type.clone()));
                            }
                        }
                        ArgumentValues::Values(list) => {
                            for v in list {
                                let boxstr: Rc<String> = Rc::from(v.to_string());
                                argument_matrix[i].push((boxstr, arguments[i].data_type.clone()));
                            }
                        }
                    }
                }

                // Choose 10 random argument combinations to test
                let mut rng = thread_rng();
                let mut argument_combinations: Vec<Vec<(Rc<dyn ArgumentValue>, String)>> =
                    Vec::new();
                for _ in 0..NUM_TESTS_PER_TASK {
                    let mut argument_combination = Vec::<(Rc<dyn ArgumentValue>, String)>::new();
                    for i in 0..argument_matrix.len() {
                        let j: usize = rng.gen_range(0..argument_matrix[i].len());
                        argument_combination.push(argument_matrix[i][j].clone());
                    }
                    argument_combinations.push(argument_combination);
                }

                let mut success = true;

                #[derive(Debug)]
                struct Failure {
                    arguments: Vec<(Rc<dyn ArgumentValue>, String)>,
                    output: String,
                    expected_output: String,
                }

                let mut errors = Vec::new();
                let mut num_passed = 0;
                for arguments in argument_combinations {
                    let (submission_output, submission_errors) = runner.run(
                        submission_path,
                        &name,
                        &output.data_type,
                        &runner.serialize_arguments(&arguments),
                    );

                    let reference_script_filename = "tmpscript.hs";
                    let mut refence_script_file =
                        fs::File::create(reference_script_filename).unwrap();
                    writeln!(refence_script_file, "{}", output.validate_function).unwrap();

                    let (reference_output, reference_errors) = runner.run(
                        reference_script_filename,
                        &name,
                        &output.data_type,
                        &runner.serialize_arguments(&arguments),
                    );
                    if !reference_errors.is_empty() {
                        eprintln!("Reference solution should run!");
                        eprintln!("{}", reference_errors);
                        unreachable!()
                    }

                    if !submission_errors.is_empty() {
                        println!(
                            "Tried to run function {:?} with arguments {:?} in ghci", name, runner.serialize_arguments(&arguments)
                        );
                        println!("Task FAILED, executing the code produced errors :(");
                        println!("Errors: {}", submission_errors);
                        success &= false;
                        errors.push(Failure {
                            output: submission_errors,
                            expected_output: reference_output,
                            arguments,
                        });
                    } else if submission_output == reference_output {
                        assert!(!submission_output.is_empty());
                        //println!("Task PASSED (matched the reference solution output) :)");
                        success &= true;
                        num_passed += 1;
                    } else {
                        success &= false;
                        errors.push(Failure {
                            output: submission_output,
                            expected_output: reference_output,
                            arguments,
                        });
                    }
                }
                println!("===============");
                if success {
                    println!("The solution for {:?} passed all tests!", name);
                    println!("Grade: {}/{}", points, points)
                } else {
                    println!(
                        "The solution for {:?} didn't (fully) pass and produced the following errors:",
                        name
                    );
                    for error in errors {
                        println!("Error while calling with arguments {}", runner.serialize_arguments(&error.arguments));
                        println!("Student's solution's output:");
                        println!("{}", error.output);
                        println!("Expected output:");
                        println!("{}", error.expected_output);
                    }

                    println!("{} of {} tests passed.", num_passed, NUM_TESTS_PER_TASK);
                    let ratio = num_passed as f32 / NUM_TESTS_PER_TASK as f32;
                    let reached_points = (points as f32 * ratio).ceil();
                    println!("Grade: {}/{}", reached_points, points)
                }
                println!("===============");
            }
        }
    }
}
