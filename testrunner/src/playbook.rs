use serde::Deserialize;

#[derive(Deserialize, Debug)]
#[serde(untagged)]
pub enum ArgumentValues {
    Range { from: i64, to: i64 },
    Values(Vec<String>),
}

#[derive(Deserialize, Debug)]
pub struct Argument {
    pub data_type: String,
    pub values: ArgumentValues,
}

#[derive(Deserialize, Debug)]
pub struct Output {
    pub data_type: String,
    pub validate_function: String,
}

#[derive(Deserialize)]
#[serde(tag = "type")]
#[serde(rename_all = "kebab-case")]
pub enum Task {
    Function {
        points: u32,
        name: String,
        arguments: Vec<Argument>,
        output: Output,
    },
}

#[derive(Deserialize)]
pub struct Assignment {
    pub tasks: Vec<Task>,
}
